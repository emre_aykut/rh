<!DOCTYPE html>
<html lang="en">
  
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="RH - KİSYP">
	<meta name="keywords" content="RH - KİSYP">

	<title>RH</title>

	<?php include('inc/head.php'); ?>
	<style type="text/css">footer{position: absolute;}</style>
	
</head>

<body class="bg-9a9a9a">

	<section class="auth-page-area">

		<div class="auth-box">
			
			<div class="text-area">
				<div class="title">KAYIT FORMU</div>
				<div class="detail">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer ce the 1500s, when an unknown printer.
				</div>
			</div>

			<form action="#" id="register-form" class="auth-form">
				<div class="special-form-group">
					<input type="text" name="fullname" id="fullname" class="special-input" placeholder="Adınız, Soyadınız" />
				</div>

					<div class="special-form-group">
					<input type="text" name="phone" id="phone" class="special-input" placeholder="Telefon" />
				</div>

				<div class="special-form-group">
					<input type="email" name="email" id="email" class="special-input" placeholder="Mail Adresi" />
				</div>

				<div class="special-btn-area">
					<button class="special-btn light-btn-bg">Kayıt Ol</button>
				</div>
			</form>

		</div>

	</section>

	<?php include('inc/footer.php'); ?>
    <?php include('inc/script.php'); ?>

</body>
  
</html>