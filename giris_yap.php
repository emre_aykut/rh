<!DOCTYPE html>
<html lang="en">
  
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="RH - KİSYP">
	<meta name="keywords" content="RH - KİSYP">

	<title>RH</title>

	<?php include('inc/head.php'); ?>
	<style type="text/css">footer{position: absolute;}</style>
	
</head>

<body class="bg-9a9a9a">

	<section class="auth-page-area">

		<div class="auth-box">
			
			<div class="text-area">
				<div class="title">Giriş Yap</div>
			</div>

			<form action="#" id="login-form" class="auth-form">
				<div class="special-form-group">
					<input type="text" name="phone" id="phone" class="special-input" placeholder="Telefon" />
				</div>

				<div class="special-form-group">
					<input type="password" name="password" id="password" class="special-input" placeholder="Şifre" />
				</div>

				<div class="special-btn-area">
					<button class="special-btn dark-btn-bg">Giriş Yap</button>
				</div>
			</form>

		</div>

	</section>

	<?php include('inc/footer.php'); ?>
    <?php include('inc/script.php'); ?>

</body>
  
</html>