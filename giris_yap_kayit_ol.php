<!DOCTYPE html>
<html lang="en">
  
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="RH - KİSYP">
	<meta name="keywords" content="RH - KİSYP">

	<title>RH</title>

	<?php include('inc/head.php'); ?>
</head>

<body>

	<section class="auth-page-area">

		<div class="auth-box">
			
			<div class="text-area">
				<div class="big-title">
					<b>Lorem Ipsum</b> is simply dummy text of the and typesetting <i>printing</i>
				</div>
				<div class="title">
					Lorem Ipsum is simply dummy text of the printing and types etting industr korem Ipsum has been the industry’s standard dummy text ever simply
				</div>
				<div class="detail">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</div>
			</div>

			<div class="auth-form">

				<div class="btn-box">
					<div class="special-btn-area">
						<a href="#" class="special-btn dark-btn-bg">Giriş Yap</a>
					</div>
				</div>
				<div class="btn-box">
					<div class="special-btn-area">
						<a href="#" class="special-btn light-btn-bg">Kayıt Ol</a>
					</div>
				</div>

			</div>

		</div>

	</section>

	<?php include('inc/footer.php'); ?>
    <?php include('inc/script.php'); ?>

</body>
  
</html>