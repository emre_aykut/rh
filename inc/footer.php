
<footer>
	<div class="container">
		<div class="row">
			
			<div class="col-md-4">
				<div class="logo">
					<img src="">
				</div>
			</div>

			<div class="col-md-4">
				<div class="copyright">
					© 2019 Sosyal Yardımlaşma
				</div>
			</div>

			<div class="col-md-4">
				<div class="submenu">
					<a href="#">Hakkımızda</a>
					<a href="#">Yardım</a>
					<a href="#">İletişim</a>
				</div>
			</div>

		</div>
	</div>
</footer>