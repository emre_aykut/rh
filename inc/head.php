<link rel="shortcut icon" href="/assets/frontend/img/fav-icon.png">

<link href="/assets/plugins/bootstrap-v4/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/frontend/css/animate.css" rel="stylesheet">
<link href="/assets/frontend/font/stylesheet.css" rel="stylesheet">
<link href="/assets/frontend/css/style.css" rel="stylesheet">

<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->