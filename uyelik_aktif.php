<!DOCTYPE html>
<html lang="en">
  
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="RH - KİSYP">
	<meta name="keywords" content="RH - KİSYP">

	<title>RH</title>

	<?php include('inc/head.php'); ?>
	<style type="text/css">footer{position: absolute;}</style>
	
</head>

<body class="bg-9a9a9a">

	<section class="auth-page-area">

		<div class="auth-box">
			
			<div class="text-area">
				<div class="membership-active-text">
					<b>John Doe,</b> üyeliğiniz aktive edilmiştir.<br />
					<i>Teşekkür ederiz.</i>
				</div>
			</div>

			<div class="auth-form">

				<div class="special-btn-area">
					<a href="" class="special-btn dark-btn-bg">Anasayfaya Dön</a>
				</div>

			</div>

		</div>

	</section>

	<?php include('inc/footer.php'); ?>
    <?php include('inc/script.php'); ?>

</body>
  
</html>